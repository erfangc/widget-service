package io.github.erfangc.widgetservice;

import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ApiController {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public ApiController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @GetMapping("{id}")
    public Widget get(@PathVariable String id) {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", id);
        return jdbcTemplate.queryForObject("SELECT * FROM widgets WHERE id = :id",
                paramMap,
                (resultSet, i) -> new Widget()
                        .setId(resultSet.getString("id"))
                        .setName(resultSet.getString("name")));
    }

    @PostMapping
    public ResponseEntity<?> set(@RequestBody Widget widget) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", widget.getId());
        paramMap.put("name", widget.getName());
        jdbcTemplate.update("INSERT INTO widgets (id, name) VALUES (:id, :name)", paramMap);
        return ResponseEntity.ok(widget.getId());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", id);
        jdbcTemplate.update("DELETE FROM widgets WHERE id = :id", paramMap);
        return ResponseEntity.ok(id);
    }
}
