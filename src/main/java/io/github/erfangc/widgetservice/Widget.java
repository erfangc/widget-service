package io.github.erfangc.widgetservice;

public class Widget {

    private String id;

    private String name;

    public Widget() {
    }

    public String getId() {
        return id;
    }

    public Widget setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Widget setName(String name) {
        this.name = name;
        return this;
    }
}
