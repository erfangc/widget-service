package io.github.erfangc.widgetservice;

import org.h2.Driver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@SpringBootApplication
public class WidgetServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WidgetServiceApplication.class, args);
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        // persist on cluster if running in Linux
        String url = "jdbc:h2:/opt/data/h2";
        final String os = System.getProperty("os.name");
        if (os.startsWith("Mac OS")) {
            // when running locally use in-memory implementation
            url = "jdbc:h2:mem";
        }
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(new SimpleDriverDataSource(new Driver(), url));
        // initialize the database schema
        jdbcTemplate.update("CREATE TABLE IF NOT EXISTS widgets (id VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL)");
        return jdbcTemplate;
    }

}
