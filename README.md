# Widget Service

The code in this repo is representative of a typical containerized application.

 - This application uses a Dockerfile to package the built artifact
 - Aside from the Dockerfile, the application has no knowledge of the Kubernetes cluster (ex: there is no `deployment.yaml` or Helm charts in this repo)
 - The fact it is written in Java using Spring Boot is irrelevant to whoever end up deploying it 
 
>In an effort to demonstrate GitOps, the image produced by this service will be deployed to a Kubernetes cluster. However, the operation manifest files (deployment template, service, volume claim etc) are stored in the development environment's operation manifest repository: [TODO]()

## Running Locally

### Using Docker

If you have docker installed on your local computer, you can test the application locally by running docker:

```bash
docker run erfangc/widget-service:master
```

If you do not have docker installed you can run the project as a ordinary maven project 

### Using Maven

```bash
./mvnw spring-boot:run
```