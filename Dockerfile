# assuming your POM's build.finalName property is set to portfolios-service
FROM openjdk:8-jdk-slim

ADD target/widget-service.jar /

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/widget-service.jar"]